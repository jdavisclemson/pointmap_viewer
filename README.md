# Pointmap Viewer

## Purpose

The purpose of this software is to provide a free and open source tool, to view "pointmaps" in 3D.
A pointmap is a visual tool, used by a metrology team to communicate the inspection locations in an automated part inspection routine.

## Examples

To demonstrate the functionality of the software, 3 example pointmaps are provided with it.
The pointmap viewer will default to this directory when you attempt to open a file, unless you configure it otherwise.

## Creating a pointmap
<pre>

To create a pointmap, you will need to produce one STL file of your model, and one text file, which contains the point data.
The coordinate system and scale must be the same between the 2 files.
The files must have the following names: "model.stl" and "points.txt".
The points.txt file contains lines of data, where each line is either a point, is blank, or is a comment.
If the points.txt file contains a line that does not fall into one of these three categories, the pointmap viewer will simply write an error to stdout, and skip over that line.
Comments are lines that are preceeded by "//".
The format for a point is as follows (without the quotes): 
<pre>"Point Name",X,Y,Z,I,J,K</pre>
The point name must include exactly 1 underscore.
The part of the name that preceeds the underscore is the "characteristic name".
This will display next to a checkbox control, when the pointmap is loaded.
It is basically the name of a group of points, and the checkbox control can be used to toggle the visibility of that group on and off.
The characteristic name has no formatting rules, except that it should only contain alphanumeric characters.
Other characters are not explicitly forbidden, but they are not guaranteed to function properly.
The part of the name that follows the underscore must contain exactly 1 number (which can be multiple digits), and the number must be at the end.
This number is the "point number", or the point's index within its characteristic, or group. The pointmap viewer will ignore any characters between the underscore, and this number.
The coordinates must be numeric, and exponential notation is supported. Scientific notation is not supported though, as points are unlikely to export from a given software in this format.
An example point is shown below. This is an example of the 2nd point within the point group CN0301 (short for characteristic number 0301). Specifically, this point was taken from the wankel_rotor.pmp example pointmap file.
<pre>CN0301_PNT2,90.04603,0.006060000000000613,38,1,-1.224675584420926e-16,-0</pre>
Once you have the model.stl and points.txt file, create a zip archive with them.
Do not add them to a folder and then zip the folder. Instead, you should highlight both files, right-click and zip them (or the equivalent via the CLI). They must be in the root of the zip archive.
Once you have the zip archive, change the file extension from ".zip" to ".pmp".
Note that you must use the zip compression algorithm. The pointmap viewer will attempt to extract the file with the "unzip" command.
This completes the pointmap.
Note that you can use the deconstruct the example pointmap files to use for reference.
To do so, just change the extension from ".pmp" to ".zip", and extract (or the equivalent via the CLI).
</pre>

## Acknowledgements
<pre>

I would like to thank the GrabCAD users "Fakhri Kerkeni", "enrico maurizi", and "An Engineer" for the CAD models used in the pointmap examples.
The pointmap CAD models can be found at the links below:
  boat_propeller.pmp:  https://grabcad.com/library/barreled_hub_conic_seal_propeller-1
  artificial_hip.pmp:  https://grabcad.com/library/hip-replacement-2
  wankel_rotor.pmp:    https://grabcad.com/library/wankel-engine-rotor-wankel-motor-rotoru-triangle-piston-1
</pre>

## Testing and known bugs
<pre>

Development was performed on Pop!_OS 20.04 LTS x86_64. Testing was performed on Pop!_OS 20.04 LTS x86_64, Ubuntu 20.04.3 LTS x86_64, and Manjaro XFCE 21.1.4. 
  On standard Ubuntu (using gnome shell), the Qt library does not account for the width of the dock, when it is displayed vertically. This causes the windows to overlap a bit on start, but it is non-critical, and it is an upstream issue.
</pre>

## Dependencies
<pre>

Dependencies are outlined below. Note that there are some dependencies which are not listed, 
  as they are generic linux packages/commands, and can be expected to exist. 
  If you are missing any of them, an error should be displayed, either during compilation, or at runtime.

Below are the notable dependencies:

  These dependencies are needed to compile the software, and all except cmake are required at runtime:
    1) g++ (Any recent version with C++17 support should work. Note that on some OS's, this is part of the gcc package, and on others, it is separate. It's good practice to ensure that gcc and g++ are both installed.)
    2) cmake (version 3.16 or later)
    3) make
    On Pop!_OS and Ubuntu:
      4) qtbase5-dev (testing done with version 5.12.8+dfsg-0ubuntu1)
    On Manjaro:
      4) qt5-base (testing done with version 5.15.2+kde+r237-1)
    
  These commands/packages are needed to run the software:
    1) openscad
    2) pkill
    3) unzip
    4) touch
</pre>

## Building/Installing the software
<pre>

To build the software, open a terminal in the software's root directory.
  Run the build.sh script. Note that you may have to make it executable with "chmod +x build.sh".
  This will build the pointmap_viewer executable and place it in the software's root directory.
  
Note that the pointmap_viewer executable relies on files and directories in its current directory, at runtime.
  These files exist in the software's root directory.
  The pointmap_viewer executable cannot be cleanly moved to an existing part of the path, 
  as this would separate it from its dependencies, or place dependent files in directories they shouldn't be in.
  If you want to be able to call the pointmap_viewer from any directory in the terminal, 
  it is recommended to either create a symlink to the executable in an existing part of the path, 
  or add the software's root directory to the "$PATH" by editing the ".bashrc" file.
  
Lastly, you can use the pointmap_viewer.png file as an icon, should you choose to add the pointmap_viewer to your desktop environment's menu.
</pre>
