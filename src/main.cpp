//This file is distributed under the GNU Lesser General Public License v3.0.
//  A copy of this license can be found in the root directory of this project.

#include <QApplication>
#include "mainWindow.h"
#include <libgen.h> // dirname
#include <unistd.h> // readlink
#include <linux/limits.h> // PATH_MAX

using namespace std;

int main(int argc, char *argv[])
{
    char fpath[PATH_MAX];
    ssize_t numchars = readlink("/proc/self/exe", fpath, PATH_MAX);
    if (numchars == -1) return 1;
    string rootdir(dirname(fpath));
    rootdir += "/";
    string homedir = getenv("HOME");
    homedir += "/";
    QApplication app(argc, argv);
    MainWindow mw(rootdir, homedir);
    mw.show();
    return app.exec();
}
