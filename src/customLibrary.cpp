//This file is distributed under the BSD 3-Clause license.
//  A copy of this license can be found in the root directory of this project.

#include "customLibrary.h"

namespace fs = std::experimental::filesystem;

using namespace std;
using namespace cl;

//Constructor definition for stringHashSet...
cl::stringHashSet::stringHashSet(){}

//Destructor definition for stringHashSet...
cl::stringHashSet::~stringHashSet(){}

//Method to check for existence of an item in the set...
bool cl::stringHashSet::containsItem(const string &item){
    list<string> lt = ht[item];
    for(list<string>::iterator it = lt.begin(); it != lt.end(); it++){
        if((*it) == item) return true;
    }
    return false;
}

//Method to add an item to the hash set without checking to see if it already exists...
//  This method is faster than the checking counterpart, and should be used when the calling function manages duplicates,
//  or when the performance cost of duplicates is not expected to exceed the cost of checking.
void cl::stringHashSet::addItemNoCheck(const string &item){
    ht[item].push_back(item);
}

//Method to add an item to the hash set, only if it doesn't already exist...
//  This method is relatively slow, but prevents duplicates.
void cl::stringHashSet::addItem(const string &item){
    if(containsItem(item)) return;
    addItemNoCheck(item);
}

//Method to remove an item from the hash set...
void cl::stringHashSet::removeItem(const string &item){
    ht[item].remove(item);
}

//Method to trim the left side of a string...
void cl::leftTrim(string &line, const string &trimString){
    if(trimString.length() > line.length() || trimString.empty()) return;
    while(line.substr(0, trimString.length()) == trimString) line.erase(0, trimString.length());
}

//Method to trim the right side of a string...
void cl::rightTrim(string &line, const string &trimString){
    if(trimString.length() > line.length() || trimString.empty()) return;
    while(line.substr(line.length() - trimString.length(), trimString.length()) == trimString) line.erase(line.length() - trimString.length(), trimString.length());
}

//Method to trim both sides of a string...
void cl::trim(string &line, const string &trimString){
    cl::leftTrim(line, trimString);
    cl::rightTrim(line, trimString);
}

//Method to determine if a string is numeric...
bool cl::isNumeric(const string &line){
    int eCount = 0;     //Variable to count the number of times "e/E" is encountered, to accommodate exponential notation.
    bool dashOk;        //Status variable to track whether or not it's okay for a given character to be a "-". This is set at the beginning of each loop iteration.
    bool dotOk = true;  //Status variable to track whether or not it's okay for a given character to be a ".". This is true by default, but may get turned off in the loop.
    for(const_s_iter it = line.begin(); it != line.end(); it++){
        if(it == line.begin()) dashOk = true;                     //It's okay to have a "-" as the first character, as the number may be negative.
        else if(*(it-1) == 'e' || *(it-1) == 'E') dashOk = true;  //It's okay to have a "-" after an "e/E", since it may be a small number in exponential format.
        else dashOk = false;                                      //Otherwise, it's not okay for the character to be a dash.
        if(isdigit(*it)) continue;                                                   //Digits are always okay.
        if(*it == '-' && dashOk) continue;                                           //If the character is a "-", and dashes are okay, then continue.
        if(*it == '.' && dotOk){ dotOk = false; continue; }                          //If the character is a "." and dots are okay, then continue. But first, turn off the dotOk variable, since there should be no more than one instance of this character.
        if((*it == 'e' || *it == 'E') && eCount++ == 0){ dotOk = false; continue; }  //If this is the first instance of an "e/E", then continue. But first, increment the eCount variable, and turn off dotOk, since only integers should follow the "e/E".
        return false;
    }
    //Lastly, if an instance of "e/E" was found, make sure there are digits on either side of it.  Otherwise, the string is not numeric.
    if(eCount){
        const_s_iter first_digit = find_if(line.begin(), line.end(), [=](char ch){ return isdigit(ch); });
        const_s_iter last_digit = find_if(line.rbegin(), line.rend(), [=](char ch){ return isdigit(ch); }).base();
        const_s_iter e = find_if(line.begin(), line.end(), [=](char ch){
            if(ch == 'e' || ch == 'E') return true;
            return false;
        });
        if(first_digit > e || e >= last_digit) return false;
    }
    return true;
}

//Method to find and replace within a string...
string cl::findAndReplace(const string &input, const string &oldstring, const string &newstring){
    if(!oldstring.length()) return input;
    string output;
    const_s_iter loc1 = input.begin();
    while(loc1 != input.end()){
        string test;
        copy(loc1, loc1 + oldstring.length(), back_inserter(test));
        if(test == oldstring){
            output += newstring;
            loc1 += oldstring.length();
        }else{
            copy(loc1, loc1 + 1, back_inserter(output));
            loc1++;
        }
    }
    return output;
}

//Method to split a string to a vector by a delimiter in one, readable line...
//  Note that this method also allows the delimiter to contain multiple characters.
vector<string> cl::splitString(const string &input, const string &delimiter){
    vector<string> output;
    const_s_iter loc1 = input.begin();
    string entry;
    while(loc1 != input.end()){
        string test;
        copy(loc1, loc1 + delimiter.length(), back_inserter(test));
        if(test == delimiter){
            output.push_back(entry);
            entry.clear();
            loc1 += delimiter.length();
        }else{
            copy(loc1, loc1 + 1, back_inserter(entry));
            loc1++;
        }
    }
    if(!entry.empty()) output.push_back(entry);
    return output;
}

//Method to escape special characters in a file path...
string cl::escapePath(string input){
    string output = findAndReplace(findAndReplace(input, " ", "\\ "), "\\\\", "\\");
    output = findAndReplace(findAndReplace(output, "(", "\\("), "\\\\", "\\");
    output = findAndReplace(findAndReplace(output, ")", "\\)"), "\\\\", "\\");
    return output;
}

//Method to check whether or not a command is installed, and maybe write a warning...
bool cl::checkCommand(string cmd, bool notFoundWarning){
    string path = getenv("PATH");
    vector<string> paths = splitString(path, ":");
    for(vs_iter it = paths.begin(); it != paths.end(); it++){
        string test = (*it) + "/" + cmd;
        if(fileExists(test)) return true;
    }
    if(notFoundWarning) cout << "WARNING: " << cmd << " not found in $PATH." << endl;
    return false;
}

//Method to check whether or not a path exists...
bool cl::pathExists(string fnam){
    fs::path p(fnam.c_str());
    return fs::exists(p);
}

//Method to check whether or not a file exists...
bool cl::fileExists(string fnam){
    fs::path p(fnam.c_str());
    return(fs::exists(p) && !fs::is_directory(p));
}

//Method to check whether or not a directory exists...
bool cl::dirExists(string fnam){
    fs::path p(fnam.c_str());
    return(fs::exists(p) && fs::is_directory(p));
}

//Method to ensure that a directory exists...
bool cl::ensureDirExists(string fnam){
    fs::path p(fnam.c_str());
    if(!fs::exists(p)) fs::create_directory(p);
    return dirExists(fnam);
}
