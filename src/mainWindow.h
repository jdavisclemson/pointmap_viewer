//This file is distributed under the GNU Lesser General Public License v3.0.
//  A copy of this license can be found in the root directory of this project.

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#define _GLIBCXX_USE_NANOSLEEP 1 //This definition is here to work around a bug that's in some versions of the thread library.
#include <thread>

QT_BEGIN_NAMESPACE
class QLineEdit;
class QMenuBar;
class QVBoxLayout;
class QGridLayout;
class QHBoxLayout;
class QThread;
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
    public:
        MainWindow();
        MainWindow(std::string, std::string);
    private:
        //Define a struct to hold user-adjustable settings, and declare an instance of it.
        struct settingsStruct{
            std::string defaultPMPDirectory;
            double cadColor[4];
            double pointColor[4];
            double textColor[4];
            double pointSize;
            double textSize;
            double textOffset;
            int textAutoOrient;
            int autoOrientInterval;
            int omitSingleCNName;
            int maxCNsPerRow;
        };
        settingsStruct settings;
        //Global QThread to run the updateFileLoop.
        std::atomic<QThread *> th;
        //Global control declarations...
        QVBoxLayout *mainLayout;
        QMenuBar *menuBar;
        QGridLayout *checkboxLayout;
        QHBoxLayout *topControls;
        QLineEdit *pnumFilterLineEdit;
        //Global variable declarations/definitions...
        std::map<std::string, std::map<int, std::vector<double>>> CNs;
        std::string rootdir;
        std::string homedir;
        std::string pointmapViewerWorkingDir;
        std::string settingsPath;
        std::string openSCADConfigDir;
        std::string openSCADWorkingDir;
        std::string scad_filepath;
        std::string windowTitle;
        const std::string icon_filename = "pointmap_viewer.png";
        const std::string point_filename = "points.txt";
        const std::string model_filename = "model.stl";
        const std::string scad_filename = "pmap.scad";
        const std::string defaultWindowTitle = "Pointmap Viewer";
        const std::string openscad_config_filename = "OpenSCAD.conf";
        const std::string openscad_backup_config_filename = "OpenSCAD.conf.bak";
        double centroid[3];
        double boundingRadius;
        int ScreenWidth;
        int ScreenHeight;
        int br_to_vpd_ratio = 10;
        bool displayPnumOnly = false;
        bool pointmapOpen = false;
        std::atomic<bool> runUpdateFileLoop;
        //Function prototypes...
        void closeEvent(QCloseEvent *event) override; //Override close event so we can close OpenSCAD as well, on exit.
        void readSettings(); //Methods to read and manipulate settings...
        void setDefaultSettings();
        void editSettings();
        void deleteChildren(QWidget *); //Methods to draw the main window...
        void drawMainWindow();
        void createMenu();
        void createTopControls();
        void createGrid();
        void clearSCADFile(); //Methods for basic OpenSCAD manipulation...
        void clearSCADWorkingDir();
        void startOpenSCAD(int, int, int, int);
        void killOpenSCAD();
        void openPointmap(); //Methods for handling/rendering pointmaps...
        void closePointmap();
        void renderPointMap();
        void parsePointFile();
        void getCentroid();
        void getBoundingRadius();
        void prepareViewport();
        std::vector<std::string> getCheckedControls();
        void checkListOfControls(std::vector<std::string>);
        std::vector<int> getFilteredPnums();
        std::vector<std::vector<std::string>> getPointList();
        void setAllCheckedState(Qt::CheckState); //Method to manipulate checkbox controls in mass.
        void updateFileLoop(); //Methods for updating the point label orientation...
        void launchFileUpdater();
        void stopFileUpdater();   
};

#endif // MAIN_WINDOW_H
