//This file is distributed under the GNU Lesser General Public License v3.0.
//  A copy of this license can be found in the root directory of this project.

#include "mainWindow.h"
#include "customLibrary.h"

using namespace std;
using namespace cl;

//Constructor definitions...
MainWindow::MainWindow(){}
MainWindow::MainWindow(string rootdir_tmp, string homedir_tmp){
    //Verify that all needed commands are installed on the OS.
    int depFoundCount = 0;
    depFoundCount += checkCommand("echo", true);
    depFoundCount += checkCommand("rm", true);
    depFoundCount += checkCommand("openscad", true);
    depFoundCount += checkCommand("pkill", true);
    depFoundCount += checkCommand("unzip", true);
    depFoundCount += checkCommand("touch", true);
    if(depFoundCount < 6) cout << "The software may not function correctly with missing dependencies." << endl;
    //The library functions which get the application directory must be called from main().
    //  Therefore, the values must be passed to the constructor as arguments.
    //  In order to make them global, the argument values are assigned to globally-scoped variables.
    rootdir = rootdir_tmp;
    homedir = homedir_tmp;
    //Get various directories and paths, creating them if needed.
    pointmapViewerWorkingDir = rootdir;  //Note that these 2 directories are the same, but it's written to be easily modified.
    int errCount = 0;
    if(!ensureDirExists(pointmapViewerWorkingDir)) errCount++;
    settingsPath = pointmapViewerWorkingDir + "settings.txt";
    openSCADConfigDir = homedir + ".config/OpenSCAD/";
    if(!ensureDirExists(openSCADConfigDir)) errCount++;
    openSCADWorkingDir = pointmapViewerWorkingDir + "OpenSCAD_Working_Directory/";
    if(!ensureDirExists(openSCADWorkingDir)) errCount++;
    scad_filepath = openSCADWorkingDir + scad_filename;
    string cmd = "echo \'\' > " + escapePath(scad_filepath);
    system(cmd.c_str());
    if(!fileExists(scad_filepath)) errCount++;
    //Write the directories, paths, and filenames to stdout, for debugging/hacking.
    cout << endl;
    cout << "Application directory: " << rootdir << endl;
    cout << "Home directory: " << homedir << endl;
    cout << "Settings filepath: " << settingsPath << endl;
    cout << "OpenSCAD configuration directory: " << openSCADConfigDir << endl;
    cout << "OpenSCAD working directory: " << openSCADWorkingDir << endl;
    cout << "SCAD filename: " << scad_filename << endl;
    //Write error and close, if the software was unable to ensure the existence of directories/files.
    if(errCount) { cout << endl << "Error...Unable to ensure existence of necessary directories/files." << endl; this->close(); }
    readSettings();  //Read settings from settings file.
    //If the OpenSCAD config file exists, and no existing backup file is found, copy the config file to a backup config file.
    //  This will be used to restore the original settings when the software closes.
    string openSCADConfigPath = openSCADConfigDir + openscad_config_filename;
    string openSCADBackupConfigPath = openSCADConfigDir + openscad_backup_config_filename;
    if(fileExists(openSCADConfigPath) && !fileExists(openSCADBackupConfigPath)){
        string cmd = "cp " + escapePath(openSCADConfigPath) + " " + escapePath(openSCADBackupConfigPath);
        system(cmd.c_str());
    }
    //Get screen geometry, and set appropriate size and position for the MainWindow.
    QDesktopWidget dw;
    ScreenWidth=dw.availableGeometry(this).width();
    ScreenHeight=dw.availableGeometry(this).height();
    resize(ScreenWidth / 2 - 25 - 25 / 2, ScreenHeight - 50);
    move(ScreenWidth / 2 + 25 / 2, 25);
    //Launch OpenSCAD.
    clearSCADWorkingDir();
    startOpenSCAD(25, 25, ScreenWidth / 2 - 25 - 25 / 2, ScreenHeight - 50);
    //Bind the keyboard shortcuts.
    QShortcut *enterShortcut = new QShortcut(QKeySequence(Qt::Key_Return), this);
    enterShortcut->setAutoRepeat(false);
    connect(enterShortcut, &QShortcut::activated, this, [=](){renderPointMap();} );
    QShortcut *controlOShortcut = new QShortcut(QKeySequence(tr("Ctrl+O")), this);
    controlOShortcut->setAutoRepeat(false);
    connect(controlOShortcut, &QShortcut::activated, this, [=](){openPointmap();} );
    QShortcut *controlWShortcut = new QShortcut(QKeySequence(tr("Ctrl+W")), this);
    controlWShortcut->setAutoRepeat(false);
    connect(controlWShortcut, &QShortcut::activated, this, [=](){closePointmap();} );
    //This delay is an effort to make the main window the last item to focus.
    //  This is so the user can immediately use keyboard shortcuts.
    //  This only works if OpenSCAD starts before the delay is up.
    //  However, alternatives to this race condition were too complex to justify implementing for such a non-critical issue.
    this_thread::sleep_for(chrono::milliseconds(1000));
    //Draw the MainWindow.
    windowTitle = defaultWindowTitle;
    setWindowIcon(QIcon((rootdir + icon_filename).c_str()));
    th = nullptr; //Set th to nullptr, since the method that launches the thread expects this initial condition.
    drawMainWindow();
}

//Override the default closeEvent method with one that also closes OpenSCAD...
void MainWindow::closeEvent(QCloseEvent *event){
    killOpenSCAD();
    //Once OpenSCAD is closed, restore the original settings, if the backup config file is found.
    string openSCADConfigPath = openSCADConfigDir + openscad_config_filename;
    string openSCADBackupConfigPath = openSCADConfigDir + openscad_backup_config_filename;
    if(fileExists(openSCADBackupConfigPath)){
        string cmd = "mv " + escapePath(openSCADBackupConfigPath) + " " + escapePath(openSCADConfigPath);
        system(cmd.c_str());
    }
}

//Method to update settings variables from settings file...
void MainWindow::readSettings(){
    //Loop through settings file and store contents in a map.
    string line;
    map<string, string> settingsMap;
    ifstream in(settingsPath);
    while(in.is_open() && getline(in, line)){
        trim(line, " ");
        if(line.length() < 3 || line.substr(0, 2) == "//") continue;
        if(line.length() == 0) continue;
        vector<string> splitByEquals = splitString(line, "=");
        if(splitByEquals.size() != 2) continue;
        settingsMap[splitByEquals[0]] = splitByEquals[1];
    }
    in.close();
    //Try to set all of the settings from the map.
    try{
        settings.cadColor[0] = stod(settingsMap["cadRed"]);
        settings.cadColor[1] = stod(settingsMap["cadGreen"]);
        settings.cadColor[2] = stod(settingsMap["cadBlue"]);
        settings.cadColor[3] = stod(settingsMap["cadOpacity"]);
        settings.pointSize = stod(settingsMap["pointSize"]);
        settings.pointColor[0] = stod(settingsMap["pointRed"]);
        settings.pointColor[1] = stod(settingsMap["pointGreen"]);
        settings.pointColor[2] = stod(settingsMap["pointBlue"]);
        settings.pointColor[3] = stod(settingsMap["pointOpacity"]);
        settings.textSize = stod(settingsMap["textSize"]);
        settings.textOffset = stod(settingsMap["textOffset"]);
        settings.textColor[0] = stod(settingsMap["textRed"]);
        settings.textColor[1] = stod(settingsMap["textGreen"]);
        settings.textColor[2] = stod(settingsMap["textBlue"]);
        settings.textColor[3] = stod(settingsMap["textOpacity"]);
        settings.textAutoOrient = stoi(settingsMap["textAutoOrient"]);
        settings.autoOrientInterval = stoi(settingsMap["autoOrientInterval"]);
        settings.omitSingleCNName = stoi(settingsMap["omitSingleCNName"]);
        settings.defaultPMPDirectory = findAndReplace(settingsMap["defaultPMPDirectory"] + "/", "//", "/");
        settings.maxCNsPerRow = stoi(settingsMap["maxCNsPerRow"]);
        if(!ensureDirExists(settings.defaultPMPDirectory)) { exception ex; throw ex; }
    //If anything went wrong, activate the default settings, and display an error message to the user.
    }catch(exception){
        setDefaultSettings();
        QMessageBox *mb = new QMessageBox(this); mb->setWindowTitle("Error"); mb->setText("Error in settings file.\nUsing default settings.\nTo write defaults to file, open settings and save.");
        mb->show();
    }
}

//Method to set settings to default values...
void MainWindow::setDefaultSettings(){
    settings.cadColor[0] = 0.7;
    settings.cadColor[1] = 0.7;
    settings.cadColor[2] = 0.7;
    settings.cadColor[3] = 0.8;
    settings.pointSize = 5;
    settings.pointColor[0] = 0;
    settings.pointColor[1] = 0;
    settings.pointColor[2] = 0;
    settings.pointColor[3] = 1;
    settings.textSize = 5;
    settings.textOffset = 5;
    settings.textColor[0] = 0;
    settings.textColor[1] = 1;
    settings.textColor[2] = 0;
    settings.textColor[3] = 1;
    settings.textAutoOrient = true;
    settings.autoOrientInterval = 1500;
    settings.omitSingleCNName = 1;
    settings.defaultPMPDirectory = rootdir + "Example_Pointmaps/";
    settings.maxCNsPerRow = 7;
}

//Edit the pointmap display settings...
void MainWindow::editSettings(){
    //Create a dialog to display.
    QDialog *es = new QDialog(); es->setWindowTitle("Pointmap Viewer Settings");
    //Create the primary layouts for the dialog.
    QVBoxLayout *vl = new QVBoxLayout(es);
    QGridLayout *gl = new QGridLayout(es);
    //Add the controls to edit the default pointmap directory.
    QHBoxLayout *hl = new QHBoxLayout(es);
    QLabel *PMPDirLineEditLabel = new QLabel(es); PMPDirLineEditLabel->setText("Default Pointmap Directory:");
    QLineEdit *PMPDirLineEdit = new QLineEdit(es); PMPDirLineEdit->setText(settings.defaultPMPDirectory.c_str());
    QPushButton *browsePMPDir = new QPushButton(es); browsePMPDir->setText("Browse");
    hl->addWidget(PMPDirLineEditLabel);
    hl->addWidget(PMPDirLineEdit);
    hl->addWidget(browsePMPDir);
    vl->addSpacing(5);
    vl->addLayout(hl);
    vl->addSpacing(10);
    //Define the color slider label text, and some vectors to store the loop-generate .
    string colorLabels[] = {"Red:", "Green:", "Blue:", "Opacity:"};
    vector<QSlider*> textColorSliders, pointColorSliders, cadColorSliders;
    //Get the length of the text, point, and cad color arrays to use in loops.
    int textColorLength = sizeof(settings.textColor) / sizeof(settings.textColor[0]);
    int pointColorLength = sizeof(settings.pointColor) / sizeof(settings.pointColor[0]);
    int cadColorLength = sizeof(settings.cadColor) / sizeof(settings.cadColor[0]);
    //Add the controls to manipulate the point label display settings.
    int r = 0, c = 0;
    QLabel *textSettingsLabel = new QLabel(es); textSettingsLabel->setText("Point Label Settings:"); textSettingsLabel->setAlignment(Qt::AlignCenter);
    gl->addWidget(textSettingsLabel, r++, c, 1, 2, Qt::AlignCenter);
    for(int a = 0; a < textColorLength; a++){
        QLabel *label = new QLabel(es); label->setText(tr(colorLabels[a].c_str())); label->setAlignment(Qt::AlignCenter);
        gl->addWidget(label, r, c++);
        QSlider *slider = new QSlider(es); slider->setOrientation(Qt::Horizontal); slider->setRange(0, 100); slider->setValue(settings.textColor[a] * 100);
        gl->addWidget(slider, r++, c--);
        textColorSliders.push_back(slider);
    }
    QLabel *textSizeSliderLabel = new QLabel(es); textSizeSliderLabel->setText("Size:"); textSizeSliderLabel->setAlignment(Qt::AlignCenter);
    gl->addWidget(textSizeSliderLabel, r, c++);
    QSlider *textSizeSlider = new QSlider(es); textSizeSlider->setOrientation(Qt::Horizontal); textSizeSlider->setRange(0, 100); textSizeSlider->setValue(settings.textSize * 10);
    gl->addWidget(textSizeSlider, r++, c--);
    QLabel *textOffsetSliderLabel = new QLabel(es); textOffsetSliderLabel->setText("Offset:"); textOffsetSliderLabel->setAlignment(Qt::AlignCenter);
    gl->addWidget(textOffsetSliderLabel, r, c++);
    QSlider *textOffsetSlider = new QSlider(es); textOffsetSlider->setOrientation(Qt::Horizontal); textOffsetSlider->setRange(0, 100); textOffsetSlider->setValue(settings.textOffset * 10);
    gl->addWidget(textOffsetSlider, r++, c--);
    QCheckBox *textAutoOrientCheckBox = new QCheckBox(es); textAutoOrientCheckBox->setText("Text Auto-Orientation");
    Qt::CheckState textAutoOrientCheckState = settings.textAutoOrient ? Qt::Checked : Qt::Unchecked;
    textAutoOrientCheckBox->setCheckState(textAutoOrientCheckState);
    gl->addWidget(textAutoOrientCheckBox, r++, c, 1, 2, Qt::AlignCenter);
    QLabel *textAutoOrientIntervalSliderLabel = new QLabel(es); textAutoOrientIntervalSliderLabel->setText("Auto-Orient Interval:"); textAutoOrientIntervalSliderLabel->setAlignment(Qt::AlignCenter);
    gl->addWidget(textAutoOrientIntervalSliderLabel, r, c++);
    QSlider *textAutoOrientIntervalSlider = new QSlider(es); textAutoOrientIntervalSlider->setOrientation(Qt::Horizontal); textAutoOrientIntervalSlider->setRange(0, 100); textAutoOrientIntervalSlider->setValue(settings.autoOrientInterval / 50);
    gl->addWidget(textAutoOrientIntervalSlider, r++, c--);
    QCheckBox *textOmitSingleCNNameCheckBox = new QCheckBox(es); textOmitSingleCNNameCheckBox->setText("Omit Full Name for Single CN");
    Qt::CheckState textOmitSingleCNNameCheckState = settings.omitSingleCNName ? Qt::Checked : Qt::Unchecked;
    textOmitSingleCNNameCheckBox->setCheckState(textOmitSingleCNNameCheckState);
    gl->addWidget(textOmitSingleCNNameCheckBox, r++, c, 1, 2, Qt::AlignCenter);
    //Add the controls to manipulate the point display settings.
    r = 0; c = 2;
    QLabel *pointSettingsLabel = new QLabel(es); pointSettingsLabel->setText("Point Settings:"); pointSettingsLabel->setAlignment(Qt::AlignCenter);
    gl->addWidget(pointSettingsLabel, r++, c, 1, 2, Qt::AlignCenter);
    for(int a = 0; a < pointColorLength; a++){
        QLabel *label = new QLabel(es); label->setText(tr(colorLabels[a].c_str())); label->setAlignment(Qt::AlignCenter);
        gl->addWidget(label, r, c++);
        QSlider *slider = new QSlider(es); slider->setOrientation(Qt::Horizontal); slider->setRange(0, 100); slider->setValue(settings.pointColor[a] * 100);
        gl->addWidget(slider, r++, c--);
        pointColorSliders.push_back(slider);
    }
    QLabel *pointSizeSliderLabel = new QLabel(es); pointSizeSliderLabel->setText("Size:"); pointSizeSliderLabel->setAlignment(Qt::AlignCenter);
    gl->addWidget(pointSizeSliderLabel, r, c++);
    QSlider *pointSizeSlider = new QSlider(es); pointSizeSlider->setOrientation(Qt::Horizontal); pointSizeSlider->setRange(0, 100); pointSizeSlider->setValue(settings.pointSize * 10);
    gl->addWidget(pointSizeSlider, r++, c--);
    //Add the controls to manipulate the CAD display settings.
    r = 0; c = 4;
    QLabel *cadSettingsLabel = new QLabel(es); cadSettingsLabel->setText("Cad Settings:"); cadSettingsLabel->setAlignment(Qt::AlignCenter);
    gl->addWidget(cadSettingsLabel, r++, c, 1, 2, Qt::AlignCenter);
    for(int a = 0; a < cadColorLength; a++){
        QLabel *label = new QLabel(es); label->setText(tr(colorLabels[a].c_str())); label->setAlignment(Qt::AlignCenter);
        gl->addWidget(label, r, c++);
        QSlider *slider = new QSlider(es); slider->setOrientation(Qt::Horizontal); slider->setRange(0, 100); slider->setValue(settings.cadColor[a] * 100);
        gl->addWidget(slider, r++, c--);
        cadColorSliders.push_back(slider);
    }
    //Add controls for the main window display settings.
    r = 6; c = 4;
    QLabel *mainWindowDisplaySettingsLabel = new QLabel(es); mainWindowDisplaySettingsLabel->setText("Main window display settings:"); mainWindowDisplaySettingsLabel->setAlignment(Qt::AlignCenter);
    gl->addWidget(mainWindowDisplaySettingsLabel, r++, c, 1, 2);
    QLabel *maxCNsPerRowLineEditLabel = new QLabel(es); maxCNsPerRowLineEditLabel->setText("Max # of CN's per row:"); maxCNsPerRowLineEditLabel->setAlignment(Qt::AlignCenter);
    gl->addWidget(maxCNsPerRowLineEditLabel, r, c++);
    QLineEdit *maxCNsPerRowLineEdit = new QLineEdit(es); maxCNsPerRowLineEdit->setText(tr(to_string(settings.maxCNsPerRow).c_str()));
    gl->addWidget(maxCNsPerRowLineEdit, r++, c--);
    //Adjust the dialog geometry.
    gl->setRowMinimumHeight(0, 25);
    int leftColumnWidth = 75;
    int rightColumnWidth = 150;
    gl->setColumnMinimumWidth(0, leftColumnWidth);
    gl->setColumnMinimumWidth(1, rightColumnWidth);
    gl->setColumnMinimumWidth(2, leftColumnWidth);
    gl->setColumnMinimumWidth(3, rightColumnWidth);
    gl->setColumnMinimumWidth(4, leftColumnWidth);
    gl->setColumnMinimumWidth(5, rightColumnWidth);
    //Add the layout containing the point label, point, and cad display setting controls.
    vl->addLayout(gl);
    //Add the Close/Ok buttons to the bottom of the dialog.
    QDialogButtonBox *btnBox = new QDialogButtonBox(es); btnBox->setStandardButtons( { QDialogButtonBox::Ok, QDialogButtonBox::Close } );
    vl->addWidget(btnBox);
    es->setLayout(vl);
    //Connect the browse button to an appropriate action.
    connect(browsePMPDir, &QPushButton::clicked, this, [=](){
        QFileDialog *fd = new QFileDialog(es);
        fd->setFileMode(QFileDialog::DirectoryOnly);
        fd->setDirectory(tr(settings.defaultPMPDirectory.c_str()));
        if(fd->exec()){
            QStringList flist = fd->selectedFiles();
            if(flist.size() > 0){
                settings.defaultPMPDirectory = findAndReplace(flist[0].toStdString() + "/", "//", "/");
                PMPDirLineEdit->setText(settings.defaultPMPDirectory.c_str());
            }
        }
    });
    //Connect the close button to an appropriate action.
    connect(btnBox, &QDialogButtonBox::rejected, this, [=](){
        es->close();
        es->deleteLater();
    });
    //Connect the ok button to an appropriate action.
    connect(btnBox, &QDialogButtonBox::accepted, this, [=](){
        //Try to get settings variables from controls in dialog.
        try {
            settings.defaultPMPDirectory = findAndReplace(PMPDirLineEdit->text().toStdString() + "/", "//", "/");
            if(!ensureDirExists(settings.defaultPMPDirectory)) { exception ex; throw ex; }
            for(int a = 0; a < textColorLength; a++) settings.textColor[a] = textColorSliders[a]->value() / 100.0;
            for(int a = 0; a < pointColorLength; a++) settings.pointColor[a] = pointColorSliders[a]->value() / 100.0;
            for(int a = 0; a < cadColorLength; a++) settings.cadColor[a] = cadColorSliders[a]->value() / 100.0;
            settings.textSize = textSizeSlider->value() / 10.0;
            settings.textOffset = textOffsetSlider->value() / 10.0;
            settings.textAutoOrient = (textAutoOrientCheckBox->checkState()==Qt::Checked) ? 1 : 0;
            settings.autoOrientInterval = textAutoOrientIntervalSlider->value() * 50;
            settings.omitSingleCNName = (textOmitSingleCNNameCheckBox->checkState()==Qt::Checked) ? 1 : 0;
            settings.pointSize = pointSizeSlider->value() / 10.0;
            settings.maxCNsPerRow = stoi(maxCNsPerRowLineEdit->text().toStdString());
        //Display an error and return if anything went wrong (probably an invalid default pointmap directory).
        } catch (exception) {
            QMessageBox *mb = new QMessageBox(es); mb->setWindowTitle("Error"); mb->setText("Error...Please check your settings.");
            mb->show();
            return;
        }
        //Try to write the settings to the settings file.
        try {
            string cmd = "rm " + escapePath(settingsPath);
            system(cmd.c_str());
            ofstream out(settingsPath);
            out << "defaultPMPDirectory=" << settings.defaultPMPDirectory << endl;
            out << "textRed=" << settings.textColor[0] << endl;
            out << "textGreen=" << settings.textColor[1] << endl;
            out << "textBlue=" << settings.textColor[2] << endl;
            out << "textOpacity=" << settings.textColor[3] << endl;
            out << "textSize=" << settings.textSize << endl;
            out << "textOffset=" << settings.textOffset << endl;
            out << "textAutoOrient=" << settings.textAutoOrient << endl;
            out << "autoOrientInterval=" << settings.autoOrientInterval << endl;
            out << "omitSingleCNName=" << settings.omitSingleCNName << endl;
            out << "pointRed=" << settings.pointColor[0] << endl;
            out << "pointGreen=" << settings.pointColor[1] << endl;
            out << "pointBlue=" << settings.pointColor[2] << endl;
            out << "pointOpacity=" << settings.pointColor[3] << endl;
            out << "pointSize=" << settings.pointSize << endl;
            out << "cadRed=" << settings.cadColor[0] << endl;
            out << "cadGreen=" << settings.cadColor[1] << endl;
            out << "cadBlue=" << settings.cadColor[2] << endl;
            out << "cadOpacity=" << settings.cadColor[3] << endl;
            out << "maxCNsPerRow=" << settings.maxCNsPerRow << endl;
            out.close();
        //Display an error and return if anything went wrong.
        } catch (exception) {
            QMessageBox *mb = new QMessageBox(es); mb->setWindowTitle("Error"); mb->setText("Error writing to settings file.\nNote that the settings file is probably corrupted.\nIt would be a good idea to try to submit at least a second time.");
            mb->show();
            return;
        }
        //Now that the settings are written, read them back in.
        //  Note that this is not entirely necessary, but it ensures that the operational settings
        //  have passed the same, standard sanity check.
        //  It also forces any issues with the settings writeout to occur immediately after the cause,
        //  making it easier for the user to understand.
        readSettings();
        //Redraw the main window to make new settings active.
        //  Note that the currently checked controls and the point number filter string are collected prior to the redraw, and then set back to their original state afterwards.
        vector<string> checkedControls = getCheckedControls();
        QString filterString = pnumFilterLineEdit->text();
        drawMainWindow();
        pnumFilterLineEdit->setText(filterString);
        checkListOfControls(checkedControls);
        //Render the pointmap to make new settings active.
        //  Note that renderPointMap() will not error if no pointmap is open.
        renderPointMap();
    });
    es->show();
    //Fix the dialog's size at whatever is automatically generated.
    es->setFixedSize(es->size());
}

//Method to free the memory allocated to the QWidgets, prior to interface regeneration...
void MainWindow::deleteChildren(QWidget *w){
    QList<QObject *> objectList = w->findChildren<QObject *>();
    //Note that the loop is run backwards to avoid deleting a parent before its child, which will cause a segfault.
    for(QList<QObject *>::iterator it = objectList.end() - 1; it >= objectList.begin(); it--) (*it)->deleteLater();
}

//Method to draw (or redraw) the main window...
void MainWindow::drawMainWindow(){
    deleteChildren(centralWidget());
    setWindowTitle(tr(windowTitle.c_str()));
    mainLayout = new QVBoxLayout;
    createMenu();
    mainLayout->addSpacing(25);
    createTopControls();
    mainLayout->addSpacing(25);
    createGrid();
    mainLayout->addStretch();
    QWidget *cw = new QWidget;
    cw->setLayout(mainLayout);
    setCentralWidget(0);
    setCentralWidget(cw);
}

//Method to create the menubar and add it to the main window...
void MainWindow::createMenu(){
    menuBar = new QMenuBar();
    QMenu *fileMenu = new QMenu(tr("&File"), this);
    QAction *open = fileMenu->addAction(tr("Open"));
    connect(open, &QAction::triggered, this, [=](){openPointmap();} );
    QAction *close = fileMenu->addAction(tr("Close"));
    connect(close, &QAction::triggered, this, [=](){closePointmap();} );
    QAction *exit = fileMenu->addAction(tr("Exit"));
    connect(exit, &QAction::triggered, this, &QMainWindow::close );
    menuBar->addMenu(fileMenu);
    QMenu *editMenu = new QMenu(tr("&Edit"), this);
    QAction *editPreferences = editMenu->addAction(tr("Preferences"));
    connect(editPreferences, &QAction::triggered, this, [=](){editSettings();} );
    menuBar->addMenu(editMenu);
    mainLayout->setMenuBar(menuBar);
}

//Method to add the Select All, Clear Selection, and Point Filter QLineEdit to the top of the Pointmap Viewer...
void MainWindow::createTopControls(){
    QPushButton *selectAll = new QPushButton(this); selectAll->setText(tr("Select All")); selectAll->setMaximumWidth(125);
    QPushButton *clearSelection = new QPushButton(this); clearSelection->setText(tr("Clear Selection")); clearSelection->setMaximumWidth(125);
    QVBoxLayout *topControlsButtonLayout = new QVBoxLayout(); topControlsButtonLayout->addWidget(selectAll); topControlsButtonLayout->addWidget(clearSelection);
    QLabel *pnumFilterLabel = new QLabel(this); pnumFilterLabel->setText(tr("Point Number Filter (ex: 1,3-5,7):"));
    pnumFilterLineEdit = new QLineEdit(this); pnumFilterLineEdit->setFixedWidth(250);
    QVBoxLayout *pnumFilterLayout = new QVBoxLayout(); pnumFilterLayout->addWidget(pnumFilterLabel); pnumFilterLayout->addWidget(pnumFilterLineEdit);
    topControls = new QHBoxLayout(); topControls->addStretch(); topControls->addLayout(topControlsButtonLayout); topControls->addSpacing(15); topControls->addLayout(pnumFilterLayout); topControls->addStretch();
    mainLayout->addLayout(topControls);
    connect(selectAll, &QPushButton::clicked, this, [=](){setAllCheckedState(Qt::Checked);} );
    connect(clearSelection, &QPushButton::clicked, this, [=](){
        setAllCheckedState(Qt::Unchecked);
        pnumFilterLineEdit->clear();
    });
}

//Method to create the CN checkboxes, and add them to the main window...
void MainWindow::createGrid(){
    checkboxLayout = new QGridLayout();
    int r = 0, c = 0;
    for(auto const& [CN, pnum] : CNs){
        QCheckBox *characteristic = new QCheckBox(this); characteristic->setText(tr(CN.c_str()));
        checkboxLayout->addWidget(characteristic, r, c++);
        connect(characteristic, &QCheckBox::stateChanged, this, [=](){renderPointMap();} );
        if(c >= settings.maxCNsPerRow) {r++; c=0;}
    }
    mainLayout->addLayout(checkboxLayout);
}

//Method to clear contents from the scad file (ie, clear the OpenSCAD display and script)...
void MainWindow::clearSCADFile(){
    string cmd="echo \"\" > "+escapePath(scad_filepath);
    system(cmd.c_str());
}

//Method to clear old contents from the OpenSCAD working directory...
void MainWindow::clearSCADWorkingDir(){
    QDir dir = QDir(openSCADWorkingDir.c_str());
    QStringList list = dir.entryList();
    for(QList<QString>::iterator iter = list.begin(); iter != list.end(); iter++){
        string fnam = iter->toStdString();
        //Do not remove the current or parent directory.
        //  Also, don't remove the scad file, since OpenSCAD needs to maintain a connection to it.
        //  There is a separate method to clear the contents of that file.
        if(fnam != "." && fnam != ".." && fnam != scad_filename) dir.remove(*iter);
    }
}

//Method to launch OpenSCAD at a specified screen position and window size...
void MainWindow::startOpenSCAD(int left, int top, int width, int height){
    killOpenSCAD();  //Close any current instances of OpenSCAD.
    //Remove the OpenSCAD config file, if it exists, and locate the config file template.
    string openscad_config_templatepath = pointmapViewerWorkingDir + openscad_config_filename;
    string openscad_config_filepath = homedir + ".config/OpenSCAD/" + openscad_config_filename;
    if(fileExists(openscad_config_filepath)){
        string cmd = "rm " + openscad_config_filepath;
        system(cmd.c_str());
    }
    //Transfer the template file to the config file, replacing placeholders with variable values.
    ifstream in(openscad_config_templatepath); ofstream out(openscad_config_filepath);
    string line;
    while(in.is_open() && getline(in, line)){
        line = findAndReplace(line, "$LEFT", to_string(left));
        line = findAndReplace(line, "$TOP", to_string(top));
        line = findAndReplace(line, "$WIDTH", to_string(width));
        line = findAndReplace(line, "$HEIGHT", to_string(height));
        out << line << endl;
    }
    in.close(); out.close();
    clearSCADFile();  //Make sure nothing from a previous session executes on startup.
    //Launch OpenSCAD.
    string cmd = "openscad " + escapePath(scad_filepath) + " &";
    system(cmd.c_str());
}

//Method to kill OpenSCAD...
void MainWindow::killOpenSCAD(){
    string cmd = "pkill openscad";
    system(cmd.c_str());
}

//Method to open a pointmap...
void MainWindow::openPointmap(){
    //Open a file dialog at the default pointmap directory, and let the user select a pointmap.
    QFileDialog *ofd = new QFileDialog();
    string pointmap_filepath = ofd->getOpenFileName(this, tr("Open Pointmap"), tr(settings.defaultPMPDirectory.c_str()), tr("PMP Files (*.pmp)")).toStdString();
    ofd->deleteLater();
    //If the user selected a file, and did not cancel...
    if(pointmap_filepath != ""){
        closePointmap();  //Run this in case there is already a pointmap open.
        //Inflate the model and point file to the appropriate location.
        string cmd = "unzip " + escapePath(pointmap_filepath) + " -d " + escapePath(openSCADWorkingDir);
        system(cmd.c_str());
        //Verify that the pmp file contains the necessary components.
        string point_filepath = openSCADWorkingDir + point_filename;
        string model_filepath = openSCADWorkingDir + model_filename;
        int errCount = 0;
        if(!fileExists(point_filepath)){
            cout << endl;
            cout << "ERROR...The points file (" << point_filename << ") was not found in " << openSCADWorkingDir << endl;
            cout << "        Please make sure the " << point_filename << " file is included in the root directory of the .pmp file." << endl;
            errCount++;
        }
        if(!fileExists(model_filepath)){
            cout << endl;
            cout << "ERROR...The model file (" << model_filename << ") was not found in " << openSCADWorkingDir << endl;
            cout << "        Please make sure the " << model_filename << " file is included in the root directory of the .pmp file." << endl;
            errCount++;
        }
        if(errCount == 2){
            cout << endl;
            cout << "Also, make sure that you are using the \"zip\" compression algorithm for the .pmp file, " << endl;
            cout << "  as the pointmap_viewer uses linux's \"unzip\" command to extract the files." << endl;
        }
        if(errCount) return;
        parsePointFile();       //Parse the point file into memory.
        getCentroid();          //Get the centroid.  This is needed to center the pointmap contents on the screen, and to get the bounding radius.
        getBoundingRadius();    //Get the bounding radius.  This is used to set the initial viewport distance (zoom), and to scale point size, point label size, and point label offsets.
        prepareViewport();      //Appropriately place the point-of-view in the OpenSCAD window.
        //Redraw the main window with the appropriate checkbox controls, and the title updated to include the name of the pointmap file.
        string pointmap_filename;
        s_iter lastSlash = std::find(pointmap_filepath.rbegin(), pointmap_filepath.rend(), '/').base();
        copy(lastSlash, pointmap_filepath.end(), back_inserter(pointmap_filename));
        windowTitle = defaultWindowTitle + " - " + pointmap_filename;
        drawMainWindow();
        //Render the pointmap, and set the pointmapOpen status variable to true.
        renderPointMap();
        pointmapOpen = true;
    }
}

//Method to close the current pointmap...
void MainWindow::closePointmap(){
    clearSCADFile();
    clearSCADWorkingDir();
    CNs.clear();
    windowTitle = defaultWindowTitle;
    pointmapOpen = false;
    stopFileUpdater();
    drawMainWindow();
}

//Method to render the pointmap (ie, write the appropriate script to the scad file)...
void MainWindow::renderPointMap(){
    //Scale variables such that "5" becomes whatever the "medium" magnitude is (determined subjectively, during development).
    //  Also scale the variables to correct for the size of the pointmap.
    double pointSizeScaled = (settings.pointSize / 5) * boundingRadius / 189;
    double textSizeScaled = (settings.textSize * 2 / 5) * boundingRadius / 189;
    double textOffsetScaled = settings.textOffset * boundingRadius / 189;
    clearSCADFile(); //Clear the scad file.
    //Write the top portion of the scad script.
    ofstream pmap_file(scad_filepath);
    pmap_file << "CAD_COLOR=[" << settings.cadColor[0] << "," << settings.cadColor[1] << "," << settings.cadColor[2] << "," << settings.cadColor[3] << "];" << endl;
    pmap_file << "SPH_SIZE=" << pointSizeScaled << ";" << endl;
    pmap_file << "SPH_RES=50;" << endl;
    pmap_file << "SPH_COLOR=[" << settings.pointColor[0] << "," << settings.pointColor[1] << "," << settings.pointColor[2] << "," << settings.pointColor[3] << "];" << endl;
    pmap_file << "TXT_SIZE=" << textSizeScaled << ";" << endl;
    pmap_file << "TXT_OFFSET=" << textOffsetScaled << ";" << endl;
    pmap_file << "TXT_COLOR=[" << settings.textColor[0] << "," << settings.textColor[1] << "," << settings.textColor[2] << "," << settings.textColor[3] <<"];" << endl;
    pmap_file << "CENTROID=[" << centroid[0] << "," << centroid[1] << "," << centroid[2] << "];" << endl;
    pmap_file << "translate(-CENTROID){" << endl;
    pmap_file << "  points=[" << endl;
    //Write point array (middle portion) to the scad script.
    vector<vector<string>> points = getPointList();
    for(vvs_iter it = points.begin(); it != points.end(); it++){                            //Loop through points and produce line for scad script.
        string line = "    [\"";                                                            //Add the opening bracket and first double quote (at beginning of point name) to line.
        if(!displayPnumOnly) line += *(*it).begin() + "_PNT";                               //If the entire point name should be displayed, add the CN name and "_PNT" to line.
        line += *((*it).begin() + 1) + "\"";                                                //Add the point number and second/last double quote (at end of point name) to line.
        for(vs_iter it2 = (*it).begin() + 2; it2 != (*it).end(); it2++) line += "," + *it2;       //Append coordinate/vector data to line.
        line += "],";                                                                         //Add the closing bracket and comma to line.
        pmap_file << line << endl;
    }
    //Write the bottom portion of the scad script.
    pmap_file << "  ];" << endl;
    pmap_file << "  for(point=points){" << endl;
    pmap_file << "    pname=point[0];" << endl;
    pmap_file << "    location=[point[1],point[2],point[3]];" << endl;
    pmap_file << "    normal=[point[4],point[5],point[6]];" << endl;
    pmap_file << "    translate(location){" << endl;
    pmap_file << "      color(SPH_COLOR)sphere($fn=SPH_RES,r=SPH_SIZE);" << endl;
    pmap_file << "      color(TXT_COLOR)translate(TXT_OFFSET*normal)rotate($vpr)linear_extrude(TXT_SIZE/2)text(pname,size=TXT_SIZE,valign=\"center\",halign=\"center\");" << endl;
    pmap_file << "    }" << endl;
    pmap_file << "  }" << endl;
    pmap_file << "  color(CAD_COLOR)import(\"" << model_filename << "\");" << endl;
    pmap_file << "}";
    pmap_file.close();
    //Make sure that the file updater loop status matches the configured setting.
    if(settings.textAutoOrient) launchFileUpdater();
    else stopFileUpdater();
}

//Method to parse point file into memory...
void MainWindow::parsePointFile(){
    string fpath = openSCADWorkingDir + point_filename; //Concatenate the path to the points file.
    string improper_format_msg = "Improper point format...skipping line..."; //This error is displayed when a line in the points file has an invalid format.
    //Loop through points...
    ifstream points(fpath.c_str());
    string line;
    cout << endl;
    cout << "Reading in point data from " << fpath << ":" << endl;
    while (points.is_open() && getline(points, line)){
        //Remove spaces, and process line only if it's not a comment.
        line = findAndReplace(line, " ", "");
        if (line.length() >= 2 && line.substr(0, 2) != "//"){
            cout << "  " << line; //Write line to stdout, but don't write endl. This will come later.
            //Try to parse the line, and add its data to the CNs map.
            try{
                string CN, pnum;
                vector<double> coords;
                vector<string> splitByComma = splitString(line, ",");
                vector<string> splitByUnderscore = splitString(splitByComma[0], "_");
                if(splitByUnderscore.size() != 2) { exception ex; throw ex; }
                CN = splitByUnderscore[0];
                s_iter pnum_loc1, pnum_loc2;
                pnum_loc1 = find_if(splitByUnderscore[1].begin(), splitByUnderscore[1].end(), [](char ch){return isdigit(ch);} );
                pnum_loc2 = find_if(pnum_loc1, splitByUnderscore[1].end(), [](char ch){return !isdigit(ch);} );
                copy(pnum_loc1, pnum_loc2, back_inserter(pnum));
                if(pnum.empty() || pnum_loc2 != splitByUnderscore[1].end()) { exception ex; throw ex; }
                for(vs_iter it = splitByComma.begin() + 1; it != splitByComma.end(); it++){
                    if(!isNumeric(*it)) { exception ex; throw ex; }
                    coords.push_back(stod(*it));
                }
                if(coords.size() != 6) { exception ex; throw ex; }
                CNs[CN][stoi(pnum)] = coords; //Note that this line needs to be below any lines that may throw an exception, to keep from storing junk data.
            //If anything goes wrong, append an error message to stdout.
            } catch (exception) { cout << " : " << improper_format_msg; }
            cout << endl;  //Write endl to stdout so it prints to the terminal.
        }
    }
    //Write the contents of the CNs map (the parsed data) to stdout, for debugging purposes.
    cout << endl;
    cout << "Parsed point data:" << endl;
    for(auto const& [CN, pnums] : CNs){
        for(auto const& [pnum, pdata] : pnums){
            cout << "  " << CN << " " << pnum;
            for(const_vd_iter it = pdata.begin(); it != pdata.end(); it++) cout << " " << *it;
            cout << endl;
        }
    }
}

//Method to get the approximate centroid of the pointmap from the point coordinates...
//  Note that it assumes that the point distribution is adequately uniform and dense.
//  In general, this will be true to the extent that the error in the centroid value is negligible.
//  An alternative would be to use a special library to obtain information directly from the model,
//  but this would add an extra dependency.
void MainWindow::getCentroid(){
    int point_count = 0;
    double xsum = 0;
    double ysum = 0;
    double zsum = 0;
    for(const auto& [CN, pnums] : CNs){
        for(const auto& [pnum, pdata] : pnums){
            point_count++;
            xsum += pdata[0];
            ysum += pdata[1];
            zsum += pdata[2];
        }
    }
    centroid[0] = xsum / point_count;
    centroid[1] = ysum / point_count;
    centroid[2] = zsum / point_count;
    //Write the computed centroid to stdout for debugging purposes.
    cout << endl;
    cout << "Approximate Centroid: " << centroid[0] << " " << centroid[1] << " " << centroid[2] << endl;
}

//Get the approximate bounding radius of the pointmap from the point coordinates...
//  Note that it assumes that the centroid is adequately accurate, and that furthest point from center
//  is adequately near the true extremeties of the model.
//  In general, this will be true to the extent that the error in the bounding radius value is negligible.
//  An alternative would be to use a special library to obtain information directly from the model,
//  but this would add an extra dependency.
void MainWindow::getBoundingRadius(){
    boundingRadius = 0;
    for(const auto& [CN, pnums] : CNs){
        for(const auto& [pnum, pdata] : pnums){
            double dx = pdata[0] - centroid[0];
            double dy = pdata[1] - centroid[1];
            double dz = pdata[2] - centroid[2];
            double temp_radius = sqrt(pow(dx, 2) + pow(dy, 2) + pow(dz, 2));
            if(temp_radius > boundingRadius) boundingRadius = temp_radius;
        }
    }
    //Write the computed bounding radius to stdout for debugging purposes.
    cout << endl;
    cout << "Bounding Radius: " << boundingRadius << endl;
}

//Method to prepare OpenSCAD for a pointmap...
//  This includes:
//                 clearing the current script/display
//                 zeroing the viewport's translation and rotation
//                 setting the viewport distance (ie, zoom), to an appropriate value for the size of the model/pointmap
void MainWindow::prepareViewport(){
    double vpd = boundingRadius * br_to_vpd_ratio;
    cout << endl;
    cout << "Zeroing viewport translation and rotation..." << endl;
    cout << "Setting viewport distance to " << vpd << endl;
    clearSCADFile();
    ofstream pmap_file(scad_filepath);
    pmap_file << "$vpt=[0,0,0];" << endl;
    pmap_file << "$vpr=[0,0,0];" << endl;
    pmap_file << "$vpd=" << vpd << ";" << endl;
    pmap_file.close();
    this_thread::sleep_for(chrono::milliseconds(1000));
}

//Method to get a list of the checked controls...
vector<string> MainWindow::getCheckedControls(){
    vector<string> control_list;
    //Note that it isn't possible to iterate only a specific type of control within a layout.
    //  For this reason, you either have to iterate every control in the layout and cast it to the appropriate type,
    //  or iterate a (likely) oversized scope for a certain type of control, checking for its existence within a smaller scope.
    //  If you iterate within the layout and cast, and you encounter the wrong control type, it will segfault when you attempt to access its member functions.
    //  For this reason, the code below iterates over every QCheckBox in the MainWindow, and checks to see if it finds a valid index within the checkboxLayout layout.
    //  In the case of this software, this is not terribly inefficient, since these are the only QCheckBox controls.
    //  If this were not the case, it would be beneficial to try to minimize the scope, possibly with the use of frames.
    QList<QCheckBox*> checkBoxList = findChildren<QCheckBox*>();
    for(QList<QCheckBox*>::iterator it = checkBoxList.begin(); it != checkBoxList.end(); it++){
        if(checkboxLayout->indexOf(*it) >= 0 && (*it)->checkState() == Qt::Checked) control_list.push_back((*it)->text().toStdString());
    }
    return control_list;
}

void MainWindow::checkListOfControls(vector<string> control_list){
    //Load items from control_list into hash set to prevent nested loop.
    //  Note that this is really not necessary for performance, given the nature of this application,
    //  but it was written this way for good measure.
    stringHashSet *ht = new stringHashSet();
    for(vs_iter it = control_list.begin(); it != control_list.end(); it++){
        ht->addItemNoCheck(*it);
    }
    //See note in getCheckedControls for explanation of control looping method.
    QList<QCheckBox*> checkBoxList = findChildren<QCheckBox*>();
    for(QList<QCheckBox*>::iterator it = checkBoxList.begin(); it != checkBoxList.end(); it++){
        if(checkboxLayout->indexOf(*it) >= 0){
            if(ht->containsItem((*it)->text().toStdString())) (*it)->setCheckState(Qt::Checked);
        }
    }
    delete(ht); //Free hash set memory.
}

//Method to convert the filter string from the GUI into a list of desired point numbers...
//  Note that this method would return a list of no desired point numbers for an empty filter string,
//  but this condition is handled where getFilteredPnums is called.
vector<int> MainWindow::getFilteredPnums(){
    vector<int> filteredPnums;
    string filterString = findAndReplace(pnumFilterLineEdit->text().toStdString(), " ", ""); //Get the filter string from the GUI and remove the spaces.
    //Split the string by comma to a vector, and loop through it...
    vector<string> splitByComma = splitString(filterString, ",");
    for(vs_iter it = splitByComma.begin(); it != splitByComma.end(); it++){
        //Split each item by dash, and try to process the resulting vector...
        vector<string> splitByDash = splitString(*it, "-");
        try {
            if(splitByDash.size() == 1) { filteredPnums.push_back(stoi(splitByDash[0])); continue; }    //If there's only one item in the vector, simply add it to the output.
            else if(splitByDash.size() != 2) continue;                                                  //If the vector is empty, or contains more than 2 items, skip it, because the format is bad.
            //If the vector contains 2 items, it is a range (ie, A-B).
            //  Loop across the range and add each number to the output.
            int range_start = stoi(splitByDash[0]);
            int range_end = stoi(splitByDash[1]);
            for(int b = range_start; b <= range_end; b++){
                filteredPnums.push_back(b);
            }
        } catch(exception) { }
     }
    return filteredPnums;
}

//Method to return a list of points include in the next pointmap render...
vector<vector<string>> MainWindow::getPointList(){
    vector<vector<string>> point_list;
    vector<string> checked_controls = getCheckedControls(); //Get list of checked controls.
    //Determine whether or not to display the full point name, or just the point number.
    //  Note that this variable is not used here.  It is used by renderPointMap().
    if(settings.omitSingleCNName && checked_controls.size() == 1) displayPnumOnly = true;
    else displayPnumOnly = false;
    vector<int> filtered_pnums = getFilteredPnums(); //Get list of desired point numbers (note that an empty list means all point numbers are desired).
    //Loop through the points within the checked controls...
    for(vs_iter it = checked_controls.begin(); it != checked_controls.end(); it++){
        for(auto const& [pnum, pdata] : CNs[*it]){
            //If the desired point numbers list is empty (ie, everything is desired), or the current point number is found in the list,
            //  add the point to the output.
            if(filtered_pnums.empty() || std::find(filtered_pnums.begin(), filtered_pnums.end(), pnum) != filtered_pnums.end()){
                vector<string> point;
                point.push_back(*it);
                point.push_back(to_string(pnum));
                for(const_vd_iter it2 = pdata.begin(); it2 != pdata.end(); it2++) point.push_back(to_string(*it2));
                point_list.push_back(point);
            }
        }
    }
    return point_list;
}

//Method to check or uncheck all CNs...
void MainWindow::setAllCheckedState(Qt::CheckState state){
    for(int a = 0; a < checkboxLayout->count(); a++){
        QCheckBox *checkbox = qobject_cast<QCheckBox *>(checkboxLayout->itemAt(a)->widget());
        checkbox->setCheckState(state);
    }
}

//Method to continuously update the scad script (via the touch command)...
void MainWindow::updateFileLoop(){
    string path = escapePath(scad_filepath);
    while(runUpdateFileLoop){
        string cmd = "touch " + path;
        system(cmd.c_str());
        this_thread::sleep_for(chrono::milliseconds(settings.autoOrientInterval));
    }
}

//Method to launch the updateFileLoop method in the "th" thread...
void MainWindow::launchFileUpdater(){
    runUpdateFileLoop = true;
    if(th != nullptr) return;
    th = QThread::create( [=](){updateFileLoop();} );
    connect(th, &QThread::finished, this, [=](){ (*th).deleteLater(); th = nullptr; } );
    (*th).start();
}

//Method to stop the updateFileLoop...
void MainWindow::stopFileUpdater(){
    runUpdateFileLoop = false;
}
