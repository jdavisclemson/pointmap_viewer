//This file is distributed under the BSD 3-Clause license.
//  A copy of this license can be found in the root directory of this project.

#ifndef CUSTOMLIBRARY_H
#define CUSTOMLIBRARY_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <unordered_map>
#include <list>
#include <experimental/filesystem>

namespace cl {
    //Define a hashSet class to speed up nested loops.
    class stringHashSet{
        public:
            stringHashSet();
            virtual ~stringHashSet();
            void addItem(const std::string&);
            void addItemNoCheck(const std::string&);
            void removeItem(const std::string&);
            bool containsItem(const std::string&);
        private:
            std::unordered_map<std::string, std::list<std::string>> ht;
    };
    //Function prototypes...
    void leftTrim(std::string&, const std::string&);
    void rightTrim(std::string&, const std::string&);
    void trim(std::string&, const std::string&);
    bool isNumeric(const std::string&);
    std::string findAndReplace(const std::string&, const std::string&, const std::string&);
    std::vector<std::string> splitString(const std::string&, const std::string&);
    std::string escapePath(std::string);
    bool checkCommand(std::string, bool);
    bool pathExists(std::string);
    bool fileExists(std::string);
    bool dirExists(std::string);
    bool ensureDirExists(std::string);
    //Type definitions...
    typedef std::vector<std::vector<std::string>>::iterator vvs_iter;
    typedef std::vector<std::string>::iterator vs_iter;
    typedef std::vector<std::string>::const_iterator const_vs_iter;
    typedef std::vector<std::string>::size_type vs_size;
    typedef std::vector<double>::iterator vd_iter;
    typedef std::vector<double>::const_iterator const_vd_iter;
    typedef std::vector<double>::size_type vd_size;
    typedef std::string::iterator s_iter;
    typedef std::string::const_iterator const_s_iter;
    typedef std::string::size_type s_size;
}

#endif // CUSTOMLIBRARY_H
