#!/usr/bin/env bash

#Ensure that an empty ./build directory exists to put build files in.
rm -rf build
mkdir ./build
cd ./build
#Use the ./build directory to compile the pointmap_viewer, and then move it back to the root directory.
cmake ..
make
mv pointmap_viewer ../
#Switch back to the root directory, and clean up the build files.
cd ..
rm -rf build
#Create the settings.txt file, with the "defaultPMPDirectory" setting pointed to the ./Example_Pointmaps directory.
defaultPMPDir=`pwd`"/Example_Pointmaps/"
echo "defaultPMPDirectory=${defaultPMPDir}" > settings.txt
#Copy the rest of the default settings from the settings_template.txt file to settings.txt.
cat settings_template.txt >> settings.txt
